from model import *

def __main__():
    weights = InitializeRandomWeights(784, 784)
    input_layer = Layer(weights, Sigmoid, 'input')

    weights = InitializeRandomWeights(784, 10)
    hidden_layer = Layer(weights, SoftMax, 'hidden')


    all_tensors = InitializeAllTensors()

    input_tensors = all_tensors[0]
    expected_tensors = all_tensors[1]

    for ind_input in range(len(input_tensors)):
        normal_tensor = []
        for val in input_tensors[ind_input]:
            normal_tensor.append(float(val) / 255)
        input_tensors[ind_input] = normal_tensor


    for ind_expected in range(len(expected_tensors)):
        normal_tensor = [0,0,0,0,0,0,0,0,0,0]
        normal_tensor[int(expected_tensors[ind_expected][0])] = 1
        expected_tensors[ind_expected] = normal_tensor



    network = Network(
        [input_layer, hidden_layer],
        input_tensors,
        expected_tensors,
        0.15,
        100
    )

    network.Train()

    return 0

__main__()
