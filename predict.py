from model import *

def __main__():
    first_hidden_layer = Layer([], Sigmoid)

    second_hidden_layer = Layer([], Sigmoid)


    input_tensors = [
        [1,1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,1,1], #0
        [0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1], #1
        [1,1,1,1,0,1,0,0,1,1,1,1,1,0,0,1,1,1], #2
        [1,1,1,0,0,1,0,1,1,0,1,1,0,0,1,1,1,1], #3
        [1,0,1,1,0,1,1,0,1,1,1,1,0,0,1,0,0,1], #4
        [1,1,1,1,0,0,1,1,1,0,0,1,0,0,1,1,1,1]  #5
    ]



    network = Network([first_hidden_layer, second_hidden_layer])
    network.InitializeWeights()
    predict_tensor = SoftMax(network.Forward([1,1,1,0,0,1,0,1,1,0,1,1,0,0,1,1,1,1]))
    print("Это цифра: ", np.argmax(predict_tensor))

    return 0

__main__()
