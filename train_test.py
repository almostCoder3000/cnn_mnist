from model import *

def __main__():
    weights = InitializeRandomWeights(18, 18)
    first_hidden_layer = Layer(weights, Sigmoid)


    weights = InitializeRandomWeights(18, 6)
    second_hidden_layer = Layer(weights, Sigmoid)


    input_tensors = [
        [1,1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,1,1],
        [0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1],
        [1,1,1,1,0,1,0,0,1,1,1,1,1,0,0,1,1,1],
        [1,1,1,0,0,1,0,1,1,0,1,1,0,0,1,1,1,1],
        [1,0,1,1,0,1,1,0,1,1,1,1,0,0,1,0,0,1],
        [1,1,1,1,0,0,1,1,1,0,0,1,0,0,1,1,1,1]
    ]
    expected_tensors = [
        [1, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 1]
    ]



    network = Network(
        [first_hidden_layer, second_hidden_layer],
        input_tensors,
        expected_tensors,
        0.15,
        300
    )

    network.Train()

    return 0

__main__()
