from math import *
import os
import csv
import random
import numpy as np
import sys
import bigfloat

# Функции активации

def Sigmoid(inputs):
    return [1 / float(1 + np.exp(-x)) for x in inputs]



# Функция нормализации ответа

def SoftMax(inputs):
    a = np.exp(inputs)
    b = float(sum(a))
    #a = np.array([bigfloat.exp(val) for val in inputs])
    #b = float(sum(a))
    return a / b


# Работа с векторами

def SubtractionTensors(first, second):
    output = []
    for ind in range(len(first)):
        output.append(first[ind] - second[ind])
    return output

#Средняя квадратичная ошибка
def MSQE(prediction_tensor, test_tensor):
    sum = 0
    n = len(prediction_tensor)
    for ind in range(n):
        sum += (test_tensor[ind] - prediction_tensor[ind]) ** 2
    return sum / n


def CrossEntropy(output, expected):
    sum = 0
    for ind in range(len(output)):
        sum += float(expected[ind]) * np.log(output[ind])
    return -1 * sum


# Вспомогательные функции


# Инициализация весов для слоя

def InitializeRandomWeights(current_size, forward_size):
    weights = []
    for ind_neuron in range(current_size):
        row_weights = []
        for ind_weight in range(forward_size):
            row_weights.append(random.uniform(-0.5, 0.5))
        weights.append(row_weights)

    return weights


# Инициализация входных и выходных векторов

def InitializeAllTensors():
    input_tensors = []
    expected_tensors = []
    fname = "dataset/train.csv"
    if os.path.exists(fname):
        with open(fname, "r") as file:
            for tensor in csv.reader(file, delimiter=','):
                input_tensors.append(tensor[1:])
                expected_tensors.append(tensor[:1])
    input_tensors.pop(0)
    expected_tensors.pop(0)
    return [input_tensors, expected_tensors]
